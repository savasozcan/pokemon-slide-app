var pokemons = [
    {
        name  : 'Bulbasaur' ,
        image : 'img/balba.jpg',
        link  : 'https://bulbapedia.bulbagarden.net/wiki/Bulbasaur_(Pok%C3%A9mon)'
    },
    {
        name  : 'Koffing',
        image : 'img/koffing.jpg',
        link  : 'https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_Ability'
    },
    {
        name  : 'Mew',
        image : 'img/mew.jpg',
        link  : 'https://bulbapedia.bulbagarden.net/wiki/Mew_(Pok%C3%A9mon)'
    },
    {
        name  : 'Pikachu',
        image : 'img/pika.png',
        link  : 'https://bulbapedia.bulbagarden.net/wiki/Pikachu_(Pok%C3%A9mon)'
    },
    {
        name  : 'Psyduck',
        image : 'img/saydek.png',
        link  : 'https://bulbapedia.bulbagarden.net/wiki/Psyduck_(Pok%C3%A9mon)'
    },
    {
        name  : 'Squirtle',
        image : 'img/sikörtıl.webp',
        link  : 'https://bulbapedia.bulbagarden.net/wiki/Squirtle_(Pok%C3%A9mon)'
    },
    {
        name  : 'Snorlax',
        image : 'img/snorlax.webp',
        link  : 'https://bulbapedia.bulbagarden.net/wiki/Snorlax_(Pok%C3%A9mon)'
    },
]

var index = 0;
var slideCount = pokemons.length;
var interval;
var settings =  {
                duration: '1000',
                random: false
            };

init(settings);


document.querySelector('.fa-arrow-circle-left').addEventListener('click', function () {
    index--;
    slideShow(index);
    console.log(index)
});
document.querySelector('.fa-arrow-circle-right').addEventListener('click', function () {
    index++;
    slideShow(index);
    console.log(index)
});
document.querySelectorAll('.arrow').forEach(function (item) {
    item.addEventListener('mouseenter', function () {
        clearInterval(interval)
    })
});
document.querySelectorAll('.arrow').forEach(function (item) {
    item.addEventListener('mouseleave', function () {
        init(settings);
    })
});

function slideShow(i) {
    index = i;

    if (i < 0) {
        index = slideCount -1;
    }
    if (i >= slideCount) {
        index = slideCount - (slideCount-1) - 1;
    }

    document.querySelector('.card-img-top').setAttribute('src', pokemons[index].image);
    document.querySelector('.card-title').textContent = pokemons[index].name;
    document.querySelector('.card-link').setAttribute('href', pokemons[index].link);
}

function init(settings) {
    var prev;

    interval = setInterval(function () {
        if (settings.random) {
            do {
                index = Math.floor(Math.random() * slideCount);
            }while (index == prev)
            prev = index;
        }else {
            if (slideCount == index + 1) {
                index = -1;
            }
            slideShow(index);
            console.log(index);
            index++;
        }
        slideShow(index)
    }, settings.duration);

}


